<?php
$val = getopt("i:");
$file = $val['i'];
list($width, $height) = getimagesize($file);
$mime = getimagesize($file)["mime"];

if($mime == "image/jpeg") $im = imagecreatefromjpeg($file);
if($mime == "image/png") $im = imagecreatefrompng($file);
if($mime == "image/webp") $im = imagecreatefromwebp($file);

$width = exec('tput cols')*4;
$height = exec('tput lines')*2;
list($width_orig, $height_orig) = getimagesize($file);

$ratio_orig = $width_orig/$height_orig;

if ($width/$height > $ratio_orig) {
   $width = $height*$ratio_orig;
} else {
   $height = $width/$ratio_orig;
}
$width = round($width)-3;
$height = round($height)-3;

$im = imagescale($im, $width, $height);
		
function printPixels($map) {
	for($y=0,$i=0;$i<count($map)/2;$y+=2,$i++) {
		for($x=0;$x<count($map[0]);$x++) {
			$top = $map[$y][$x];
			
			if(isset($map[$y+1][$x]))
				$bottom = $map[$y+1][$x];
			else
				for($i=0;$i<count($map[0]);$i++)
					$bottom[] = [000,000,000];
			echo "\x1b[38;2;".$top[0].";".$top[1].";".$top[2]."m\e[48;2;".$bottom[0].";".$bottom[1].";".$bottom[2]."m▀\x1b[0m";
		}
		echo "\n";
	}
}
		
for($y=0;$y<$height;$y++) {
	for($x=0;$x<$width;$x++) {
		$rgb = imagecolorat($im, $x, $y);
		list($r, $g, $b) = [($rgb >> 16) & 0xFF, ($rgb >> 8) & 0xFF, $rgb & 0xFF];
		$rgb = [$r, $g, $b];
		$map[$y][$x] = $rgb;
	}
}
printPixels($map);