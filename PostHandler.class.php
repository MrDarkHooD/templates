<?php
/*
define('TYPE_INT', 1);
define('TYPE_FLOAT', 2);
define('TYPE_STR', 3);
define('TYPE_BOOL', 4);
define('TYPE_ARR', 5);
define('TYPE_OBJ', 6);
define('TYPE_NULL', 7);
define('TYPE_RESOURCE', 8);

define('CHARSET_ALPHANUM', 2);
*/

class POSTHandler
{

	
    public function __construct()
    {

    }
	
	private static function validateTypeInPOST(string $key, int $type, string $value, bool $returnErrorString = false)
	{
		$variable = "&dollar;_POST[\"". $key ."\"]";
		
		switch($type)
		{
			case TYPE_STR:
				if (!is_string($value)) {
					return ["error" => $variable . " is not STR."];
				}
				
				return (string) $value;
				break;
			
			case TYPE_INT:
				if (!ctype_digit($value))
				{
					return ["error" => $variable . " is not INT."];
				}
				if ($value > PHP_INT_MAX)
				{
					return ["error" => $variable . " is bigger than PHP_INT_MAX."];
				}
				
				return (int) $value;
				break;
				
			case TYPE_BOOL:
				if ($value != "0" && $value != "1")
				{
					return ["error" => $variable . " is not BOOL."];
				}
				
				return (bool) $value;
				break;
			
			default:
				//need to make this into loggable
				return ["error" => "Type check was called for undefined type"];
				break;
		}
		
		return 0;
	}
	
	private static function validateCharset($value, int $type, int $charset, array $extraChars)
	{
		$variable = "&dollar;_POST[\"". $key ."\"]";
		
		switch($charset)
		{
			case CHARSET_ALPHANUM:
				$tmpValue = strval(str_replace($extraChars, "", $value));
				if (!ctype_alnum($tmpValue))
				{
					return ["error" => "Illegal characters found"];
				}
				break;
				
			default:
				return ["error" => "Charset not recognized"];
				break;
		}
		
		return 0;
	}

	
	public static function ValidatePOSTFields(array $fields, bool $strict = false, bool $printError = false, $printSteps = false)
	{
		$output = (object) [];
		foreach($fields as $field)
		{
			$key = $field[0];
			$required = $field[1];
			$type = $field[2];
			$charset = $field[3] ?? false;
			$extraChars = ($field[4]) ?? [];
			$rawValue = $_POST[$key] ?? null;

			if (!$strict &&
				$required &&
				($type == TYPE_BOOL || $type == TYPE_INT) &&
				empty( $rawValue )
			)
			{
				$rawValue = "0";
			}

			if ( !$required && is_null( $rawValue ) )
			{
				continue;
			}
			
			if( is_null( $rawValue ) )
			{
				if ( $printError )
				{
					echo "Failed isset for " . $key . "<br>";
				}
				return false;
			}
			
			$output->{$key} = POSTHandler::validateTypeInPOST($key, $type, $rawValue);
			
			if( isset( $output->{$key}["error"] ) )
			{
				if($printError)
				{
					echo "POSTHandler::validateTypeInPOST returned error for " . $key . "<br />\r\n";
					echo $output->{$key}["error"] . "<br />\r\n";
				}
				return false;
			}
			
			if($printSteps)
				{
					echo "Type validation and declaration for " . $key . " passed.<br />\n";
				}
			
			if ( $printSteps )
			{
				echo "&dollar;_POST[\"" . $key . "\"] = (" . gettype($output->{$key}) . ") \"" . $rawValue . "\";<br />\r\n";
			}
			
			// If I coded right, this can be empty only if $type == TYPE_STR
			if(!empty($rawValue) && $charset)
			{
				//private static function validateCharset($value, int $type, int $charset, array $extraChars)
				$charsetValidatorResult = POSTHandler::validateCharset($rawValue, $type, $charset, $extraChars);
				if (isset($charsetValidatorResult["error"]))
				{
					if($printError)
					{
						echo "Failed charset validation for " . $key . "<br />\n";
						echo $charsetValidatorResult["error"] . "<br />\r\n";
					}
					return false;
				}
					
				if($printSteps)
				{
					echo "Charset validation for " . $key . " passed.<br />\n";
				}
			}
		}
		
		return $output;
	}
}
