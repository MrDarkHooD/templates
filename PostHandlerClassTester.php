<?php
require_once("PostHandler.class.php");

define('TYPE_INT', 1);
define('TYPE_FLOAT', 2);
define('TYPE_STR', 3);
define('TYPE_BOOL', 4);
define('TYPE_ARR', 5);
define('TYPE_OBJ', 6);
define('TYPE_NULL', 7);
define('TYPE_RESOURCE', 8);

define('CHARSET_ALPHANUM', 2);

$strictMode = (isset($_GET["strict"])) ? true : false;

?>
<style>
	/* This was best way.. */
input[name="bool2"][type="radio"], #tf_bool2 {
	display: none;
}.flex {
	width: 100%;
	display: flex;
	gap: 10px;
	flex-direction: row;
    justify-content: space-between;
    align-items: flex-start;
}.flex > div {
	width: 100%;
	border: 2px solid black;
	padding: 5px;
}.flex > div.row {
	display: flex;
	flex-direction: column;
	justify-content: space-between;
}.flex > div.row > div {
	width: 100%;
	margin: auto;
}.error {
	color: red;
	font-weight: bold;
}.success {
	color: green;
	font-weight: bold;
}
</style>
<?php
$typeList = [
	1 => "TYPE_INT",
	2 => "TYPE_FLOAT",
	3 => "TYPE_STR",
	4 => "TYPE_BOOL",
	5 => "TYPE_ARR",
	6 => "TYPE_NULL",
	7 => "TYPE_RESOURCE"
];

$charsetList = [
	2 => "CHARSET_ALPHANUM"
];

$extraChars = ["_", "-"];

$postFieldsRules = [
	["str", true, TYPE_STR],
	["str2", true, TYPE_STR],
	["strAlphanum", true, TYPE_STR, CHARSET_ALPHANUM],
	["strAlphanum2", true, TYPE_STR, CHARSET_ALPHANUM],
	["strAlphanumExtra", true, TYPE_STR, CHARSET_ALPHANUM, $extraChars],
	["strAlphanumExtra2", true, TYPE_STR, CHARSET_ALPHANUM, $extraChars],
	["int", true, TYPE_INT],
	["int2", true, TYPE_INT],
	["bool", true, TYPE_BOOL],
	["bool2", true, TYPE_BOOL],
];

$defaultValues = [
	"str" => "abcABC123()[]-_.,=?:;",
	"strAlphanum" => "abcABC123",
	"strAlphanumExtra" => "abcABC123-_",
	"int" => "0123456789"
];

echo "<div class=\"flex\"><div>";
if(!$strictMode)
{
	echo "<a href=\"" . $_SERVER['REQUEST_URI'] . "?strict\">Strict mode</a>";
}
else
{
	echo "<a href=\"" . $_SERVER['REQUEST_URI'] . "\">Normal mode</a>";
}

echo "<form action=\"" . $_SERVER['REQUEST_URI'] . "\" method=\"post\">\n";
foreach ($postFieldsRules as $rules)
{
	if (isset($_POST[$rules[0]]))
	{
			$defaultValues[$rules[0]] = htmlspecialchars($_POST[$rules[0]]);
	}

	echo "\t<label for=\"" . $rules[0] . "\">" . $rules[0] . "</label>\n";
	if(in_array($rules[2], [TYPE_STR, TYPE_INT]))
	{
		echo "\t<input type=\"text\" name=\"" . $rules[0] . "\"";
		if (isset($defaultValues[$rules[0]]))
		{
			echo " value=\"" . $defaultValues[$rules[0]] . "\"";
		}
		echo "><br />\n";
	}
	if($rules[2] == TYPE_BOOL)
	{
		echo "\t<span id=\"tf_" . $rules[0] . "\">true: </span><input type=\"radio\" name=\"" . $rules[0] . "\" value=\"1\">\n";
		echo "\t<span id=\"tf_" . $rules[0] . "\">false: </span><input type=\"radio\" name=\"" . $rules[0] . "\" value=\"0\"><br />\n";
	}
}
echo "\t<input type=\"text\" name=\"bool2\" value=\"" . ($_POST[$rules[0]] ?? null) . "\"><br />\n";
echo "\t<input type=\"submit\" name=\"submit\" value=\"Submit\">\n";
echo "</form>\n";
echo "</div><div class=\"row\"><div>";
echo "<b>&dollar;variables</b>\n";
echo "<pre>\n";
echo "&dollar;extraChars = [\"_\", \"-\"];\n";
echo "&dollar;post_fields = [\n";
foreach ($postFieldsRules as $rules)
{
	echo "\t[\"{$rules[0]}\", ";
	echo ($rules[1]) ? "true" : "false";
	echo ", " . $typeList[$rules[2]];
	if (isset($rules[3]))
	{
		echo ", " . $charsetList[$rules[3]];
	}
	if (isset($rules[4]))
	{
		echo ", &dollar;extraChars";
	}
	echo "]\n";
}
echo "];\n";
echo "</pre>\n";
echo "</div><div>";
echo "<b>var_dump(&dollar;_POST);</b>\n";
echo "<pre>\n";
echo "&dollar;_POST = array (\n";
foreach ($_POST as $key => $postValue)
{
	echo "\t\"" . $key . "\" => (" . gettype($postValue) . ") \"" . htmlspecialchars($postValue) . "\",\n";
}
echo ");";
	
echo "</pre>\n</div>";

echo "<div><b>Call of class</b>\r\n<pre>\r\n";
echo "&dollar;post = PostHandler::ValidatePOSTFields(\r\n";
echo "\t&dollar;postFieldsRules,\r\n";
echo "\tstrict: " . (($strictMode) ? "true" : "false") . ",\r\n";
echo "\tprintError: true,\r\n";
echo "\tprintSteps: true\r\n);\r\n";
echo "</pre>\r\n";
echo "</div></div>";

echo "<div>\n";
echo "<b>Results from PostHandler</b>\n<pre>";
if(isset($_POST["submit"]))
{
	if(!$post = PostHandler::ValidatePOSTFields(
		$postFieldsRules,
		strict: (($strictMode) ? true : false),
		printError: true,
		printSteps: true)
	)
	{
		echo "</pre>\r\n<span style=\"color:red;font-weight:bold;\">&dollar;_POST data is invalid!</span>\r\n";
	}
	else
	{
		echo "</pre>\r\n<span style=\"color:green;font-weight:bold;\">&dollar;_POST data is valid!</span><br />\r\n";
		echo "<b>var_dump(&dollar;post)</b>\r\n<pre>";
		var_dump($post);
		echo "</pre>";
	}
}
echo "</div>";
