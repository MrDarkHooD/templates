	BITS 16

;AREA Example, CODE, READONLY
;#include "commands/print.asm"

start:
	mov ax, 07C0h		; Set up 4K stack space after this bootloader
	add ax, 288		; (4096 + 512) / 16 bytes per paragraph
	mov ss, ax
	mov sp, 4096

	mov ax, 07C0h		; Set data segment to where we're loaded
	mov ds, ax

	jmp repeat

%macro delay 1
;	mov bx, 1
;	shl bx, %1
;%%rep:
;	sub bx, 1
;	test bx, bx
;	jne %%rep
	
;	mov word [0x00007E60], 0 ;start counter
	
;	mov al, 0x36
;	out 0x43, al    ;tell the PIT which channel we're setting

;	mov ax, 11931	; 100 hz
;	out 0x40, al    ;send low byte
;	mov al, ah
;	out 0x40, al    ;send high byte
	
;	cmp word [0x00007E60], %1
;	ret
;	add word [0x00007E60], 1
	
%endmacro

%macro get_CMOS 1
	mov al, %1		; 0x00 is a second in CMOS reg
	out 0x70, al		; CMOS instruction location, insert second
	in al, 0x71			; Read CMOS clock address
;	cmp 0x0B, 0x04		; Check is RTC BCD
%endmacro 
	
bcd_seconds_to_ascii:
	get_CMOS 0x00
	shr al, 4				; read first half
	add al, 0x30			; add 30 to get ascii
	mov [0x00007E00], al 	; Store first number
	
	get_CMOS 0x00
	and al, 0xf				; read second half
	add al, 0x30			; add 30 to get ascii
	mov [0x00007E01], al 	; Store second number

	ret

bcd_minutes_to_ascii:
	get_CMOS 0x02
	shr al, 4				; read first half
	add al, 0x30			; add 30 to get ascii
	mov [0x00007E02], al 	; Store first number
	
	get_CMOS 0x02
	and al, 0xf				; read second half
	add al, 0x30			; add 30 to get ascii
	mov [0x00007E03], al 	; Store second number

	ret
	
bcd_hours_to_ascii:
	get_CMOS 0x04
	shr al, 4				; read first half
	add al, 0x30			; add 30 to get ascii
	mov [0x00007E04], al 	; Store first number
	
	get_CMOS 0x04
	and al, 0xf				; read second half
	add al, 0x30			; add 30 to get ascii
	mov [0x00007E05], al 	; Store second number
	
	
	mov al, 0x0B	; Check is clock 12 or 24 hour format
	cmp al, 1
;	je convert_hours_12_to_24
	ret

convert_hours_12_to_24:
	get_CMOS 0x04
	mov al, 1
	add [0x00007E04], al
	mov al, 2
	add [0x00007E05], al
	ret

beginning_of_line:
	mov al, 13		; Beginning of line
	int 10h
	ret
	
new_line:
	mov al, 10		; New line
	mov ah, 0x0E
	int 10h 
	call beginning_of_line
	ret

%macro print 1
	cmp word %1, 0
	mov al, %1
	mov ah, 0x0E	; Print to screen
	int 10h	
%endmacro

;print_string:			; Routine: output string in SI to screen
;	mov ah, 0Eh		; int 10h 'print char' function

;.repeat:
;	lodsb			; Get character from string
;	cmp al, 0
;	je .done		; If char is zero, end of string
;	int 10h			; Otherwise, print it
;	jmp .repeat

print_clock:
	call bcd_seconds_to_ascii

	mov al, [0x00007E06]
	cmp al, [0x00007E01]
	je repeat
	
	call bcd_minutes_to_ascii
	call bcd_hours_to_ascii
	
	print [0x00007E04]
	print [0x00007E05]
	mov al, ':'
	print al
	print [0x00007E02]
	print [0x00007E03]
	mov al, ':'
	print al
	print [0x00007E00]
	print [0x00007E01]
	
	mov al, [0x00007E01]
	mov [0x00007E06], al
	
	call beginning_of_line

repeat:
	jmp print_clock
	
	
	
	
;	delay 230
	jmp repeat

.done:
	ret


	times 510-($-$$) db 0	; Pad remainder of boot sector with 0s
	dw 0xAA55		; The standard PC boot signature
