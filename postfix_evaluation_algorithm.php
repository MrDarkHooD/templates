<?php
/*
* The following algorithm evaluates postfix expressions using a stack, with the expression processed from left to right
* https://en.wikipedia.org/wiki/Reverse_Polish_notation#Postfix_evaluation_algorithm
* Author: MrDarkHooD
* https://gitlab.com/MrDarkHooD/templates/-/blob/master/postfix_evaluation_algorithm.php
*/

$stack = [];
foreach ($token_array as $token) {
    if (!is_numeric($token)) {
        $operand_2 = array_pop($stack);
        $operand_1 = array_pop($stack);
        switch ($token) {
            case "+":
                $stack[] = $operand_1 + $operand_2;
                break;
            case "-":
                $stack[] = $operand_1 - $operand_2;
                break;
            case "*":
                $stack[] = $operand_1 * $operand_2;
                break;
            case "/":
                $stack[] = $operand_1 / $operand_2;
                break;
            case "^":
                $stack[] = $operand_1 ** $operand_2;
                break;
            case "%":
                $stack[] = $operand_1 % $operand_2;
                break;
        }
    } else {
        $stack[] = $token;
    }
}

echo end($stack);