#/bin/sh
WINDOW_ID=$(xdotool getactivewindow)
wmctrl -ir $WINDOW_ID -T "Forecast"

usage() {
	echo "Usage: $0 {location}"
	echo "When running, key options:"
	echo -e "\t[t]oday"
	echo -e "\t[w]eek (not really, 3 days)\t"
	echo -e "\t[c]omprehensive\t"
	echo -e "\t[m]oon"
}

display_info() {
    local url=$1
    while :; do
        clear
        curl "$url"
        sleep 600
    done
}

while [ $# -gt 0 ]; do
	case $1 in
		-h | --help)
			usage
			exit 0
			break
			;;
		*)
			location=$1
			break
			;;
	esac
	shift
done

tput civis
url="wttr.in/${location}?1Fq"

clear
while :
do
	curl $url
	rows=$(tput lines)
	printf "\033[%d;1H" "$rows"
	echo "[t]oday, [w]eek (not), [c]omprehensive, [m]oon"

	read -n 1 -t 600 key

	case $key in
		m) url="wttr.in/Moon?Fq" ;;
		t) url="wttr.in/${location}?1Fq" ;;
		w) url="wttr.in/${location}?Fq" ;;
		c) url="v2.wttr.in/${location}?Fq" ;;
		*) url="wttr.in/${location}?1Fq" ;;
	esac
	clear
done
