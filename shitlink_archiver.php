<?php
/****************************
* Automatic shit link archiver
* Ment mainly for news links
* to prevent click gain
* Author: MrDarkHooD
* https://gitlab.com/MrDarkHooD/templates/-/blob/master/shitlink_archiver.php
****************************/

/*

This code, at first sight, looks like it's missing
all error handling and it's smelly. I'd like to explain:
That's not the case, like for real.
curl doesn't really need any checks to gain url;
it either success and gives new url, or fails and
gives original url. It gives absolutely 0 errors
on non-existing url:s and on "bad" errors (like timeout).
This is designed to be nice function and not brute force
internetarchive. It has very simple "backup plan":
let bad url pass. It's better to give clicks for those sites
than censor them. It's also better to let smelly links pass
instead of giving failure and error message to end user.

It has couple cleanups:
1. it doesn't sniff same link multiple times
2. it doesn't replace failed links with string(0) ""
*/

if( !function_exists("json_validate") )
{
	 // PHP 8.3.0 or newer has this as build in function
	function json_validate(string $string): bool
	{
	    json_decode($string);
	    return json_last_error() === JSON_ERROR_NONE;
	}
}

function linksToArchive( string $string ): string
{
	$archive_urls = [
		"iltalehti.fi",
		"is.fi",
		"seiska.fi",
		"mvlehti.net",
	];

	preg_match_all(
		'%(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})%',
		$string, $urls
	);

	$archivable_urls = [];
	foreach($archive_urls as $archive_url)
	{
		// Note that preg_quote() is not meant to be applied to the $replacement string(s) of preg_replace() etc.
		$found = preg_grep( "/" . preg_quote($archive_url) . "/", $urls[0] );
		if( count($found) )
			$archivable_urls = array_merge($archivable_urls, $found);
	}

	$archivable_urls = array_unique($archivable_urls);

	if( !count($archivable_urls) )
		return $string;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT_MS, 10000);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_USERAGENT, "Automated link archiver bot");

	$replacements = [];
	foreach( $archivable_urls as $url )
	{
		$encoded_url = urlencode($url);

		// Check if already archived
		// https://archive.org/help/wayback_api.php
		curl_setopt($ch, CURLOPT_URL, "https://archive.org/wayback/available?url=" . $encoded_url);
		$response = curl_exec($ch);

		if($response === false)
			break;

		if( json_validate($response) )
		{
			$result = json_decode($response)->archived_snapshots;
			if( isset($result->closest) )
			{
				$replacements[$url] = $result->closest->url;
				continue;
			}
		}

		// Make request to archive
		curl_setopt($ch, CURLOPT_URL, "https://web.archive.org/save/" . $encoded_url);
		curl_exec($ch);

		$replacements[$url] = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
	}
	curl_close($ch);

	$replacements = array_filter( $replacements );
	$string = strtr( $string, $replacements );
	return $string;
}