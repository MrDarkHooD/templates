<?php
class HTML {

	private $indent = 0;

	public function printTag(
		string $tag,
		array $args = [], // [ "arg"=>"val", -..- ]
		bool $close = true,
		mixed $inner = null,
		bool $onlyclose = false
	): void
	{
		if ( $onlyclose )
		{
			$this->indent--;
			echo str_repeat("\t", $this->indent) . "</$tag>\n";
			return;
		}

		$voidTags = [
			"br",
			"hr",
			"input",
			"area",
			"base",
			"col",
			"embed",
			"img",
			"link",
			"meta",
			"param",
			"source",
			"track",
			"wbr"
		];

		echo str_repeat("\t", $this->indent) . "<$tag";
		$this->indent++;

		foreach( $args as $arg => $val )
		{
			if(count($args) >= 2)
				echo "\n" . str_repeat("\t", $this->indent);
			
			if ( $val !== null)
				echo " $arg='$val'";
			else
				echo " " . $arg; // For example autoplay and diabled

		}

		if ( in_array($tag, $voidTags) )
		{
			echo " />\n";
			$this->indent--;
			return;
		}
		
		echo ">";

		if( $close )
		{
			echo $inner . "</$tag>\n";
			$this->indent--;
			return;
		}

		echo "\n";
		return;
	}
}