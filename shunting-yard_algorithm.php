<?php
/*
* The following algorithm parses mathematical expressions to reverse postfix notation
* https://en.wikipedia.org/wiki/Shunting-yard_algorithm#The_algorithm_in_detail
* Author: MrDarkHooD
* https://gitlab.com/MrDarkHooD/templates/-/blob/master/shunting-yard_algorithm.php
*/

$output_queue = [];
$operator_stack = [];

$operators = ['+', '-', '*', '/', '%', '^'];
$precedence = [
    '(' => 0,
    ')' => 0,
    '+' => 1,
    '-' => 1,
    '*' => 2,
    '/' => 2,
    '%' => 2,
    '^' => 3,];

while (isset($token_array[0])) {
    $token = array_shift($token_array);
    if (is_numeric($token)) {
        $output_queue[] = $token;
    }
    if (in_array($token, $operators)) {
        while (in_array(end($operator_stack), $operators) ||
              $precedence[end($operator_stack)] > $precedence[$token] ||
              ($precedence[end($operator_stack)] == $precedence[$token] &&
               $token != '^')) {
            if(end($operator_stack) != '(') {
                $output_queue[] = array_pop($operator_stack);
            }
        }
        $operator_stack[] = $token;
    }
    if ($token == '(') {
        $operator_stack[] = $token;
    }
    if($token == ')') {
        while (end($operator_stack) != '(') {
            $output_queue[] = array_pop($operator_stack);
        }
        if (end($operator_stack) == '(') {
            array_pop($operator_stack);
        }
    }
}

while (count($operator_stack)) {
    $output_queue[] = array_pop($operator_stack);
}

echo implode(" ", $output_queue)."\n";