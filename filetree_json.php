<?php
header('Content-Type: application/json');

function dir_scanner ($dir) {
	$output = [];
	foreach(array_diff(scandir($dir), [".", ".."]) as $object) {
		if(is_file($dir."/".$object)) {
			$output[] = $object;
		} else {
			$output[$object] = dir_scanner($dir."/".$object);
		}
	}
	return $output;
}

echo json_encode(dir_scanner("."));