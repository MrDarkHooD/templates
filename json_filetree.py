import os
import json

def dir_scanner(path):
    output = {}
    with os.scandir(path) as dir:
        for entry in dir:
            if entry.name[0] != ".":
                if entry.is_dir():
                    output[entry.name] = dir_scanner(path + "/" + entry.name)
                if entry.is_file():
                    output[len(output)] = entry.name
    return output

print (json.dumps(dir_scanner("/www")))