#!/bin/bash
starttime=$(date +%s)



endtime=$(date +%s)
runtime=$((endtime - starttime))
if [ $runtime -lt 60 ]
then
        runtime=$runtime"s"
else
	runtime=$(echo "scale=2; $runtime/60" | bc -l)"m"
fi
echo "Runtime: "$runtime

